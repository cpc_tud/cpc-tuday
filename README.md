# CPC TUDay showcase

This repro contains stuff we can show around at the TUday, during where people interested in studying come to university and visit some groups.

At the moment there is a short trajectory of a protein, which can be viewed with VMD. I think it is a good example, because it is related to something, that everybody knows, and also MD of proteins is a big thing. Movies of folding and denaturating polymers have also been added.
