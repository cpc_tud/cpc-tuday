10000 ps in 10000 frames
Contains ONLY the protein

This is the metal binding part of the APP protein (not the whole protein and no metal), you can read about it here:
https://en.wikipedia.org/wiki/Amyloid_precursor_protein

From cleaved APP the short peptides Amyloid beta are produced. They have antimicrobial functions, but also form plaques, which are involved in the Altzheimer disease. See also:
https://en.wikipedia.org/wiki/Amyloid_beta

This can be used to show, what MD can and can not be used for (simulate a whole cell, give protein structure from sequence, ...).
